<html>
<head>
    <title>Login</title>
</head>

<body>
    <div class="d-flex justify-content-center align-items-center" style="height: 100vh;">
        <div class="text-center">
            <h3 class="mb-5">Controle Financeiro Pessoal</h3>
            <form class="text-center border border-dark p-5" method="POST">
                <h4 class="mb-5">Entrar</h4>
                <div class="form-outline mb-4">
                    <input type="email" id="email" name="email" class="form-control" />
                    <label class="form-label" for="form1Example1">E-mail</label>
                </div>
                <div class="form-outline mb-4">
                    <input type="password" id="senha" name="senha" class="form-control" />
                    <label class="form-label" for="form1Example2">Senha</label>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Enviar</button>
                <p class="red_text"><?= $error ?'Dados de acesso incorretos' : '' ?></p>
            </form>
        </div>
    </div>
</body>
</html>