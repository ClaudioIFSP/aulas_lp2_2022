<div class="container">
    <br>
    <a 
    class="btn btn-primary"
    data-mdb-toggle="collapse"
    href="#collapseExample"
    role="button"
    aria-expanded="false"
    aria-controls="collapseExample"
    >
    Nova Conta
    </a>

    <div class="collapse mt-3" id="collapseExample">
        <div class="row">
            <div class="col-md-6 mx-auto border mt-5 pt-5 pb-3">
                <form method="POST">
                    <input class="form-control" name="parceiro" type="text" placeholder="Devedor / Credor"/><br>
                    <input class="form-control" name="descricao" type="text" placeholder="Descrição"/><br>
                    <input class="form-control" name="valor" type="number" placeholder="Valor"/><br><br>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <input class="form-control" name="mes" type="number" placeholder="Mês">
                        </div>
                        <div class="col-md-6">
                            <input class="form-control" name="ano" type="number" placeholder="Ano">
                        </div>
                    </div>
                    
                    <input name="tipo" type="hidden" value="<?= $tipo?>"/><br>
                    <button class="btn btn-primary" type="submit">Enviar</button>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <?= $lista ?>
        </div>
    </div>
</div>